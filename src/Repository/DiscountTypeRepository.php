<?php

namespace App\Repository;

use App\Entity\DiscountType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DiscountType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DiscountType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DiscountType[]    findAll()
 * @method DiscountType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiscountTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountType::class);
    }

    // /**
    //  * @return DiscountType[] Returns an array of DiscountType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DiscountType
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
